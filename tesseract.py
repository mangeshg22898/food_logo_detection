import cv2
import os
import numpy as np
import pytesseract
from PIL import Image

# Read the image with text
image = cv2.imread("image.jpg")

# Resize the image
image = cv2.resize(image, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)

# Convert to grayscale image
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Apply image preprocessing
gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

# Additional preprocessing: Remove noise from the image
kernel = np.ones((1, 1), np.uint8)
gray = cv2.dilate(gray, kernel, iterations=1)
gray = cv2.erode(gray, kernel, iterations=1)

# Write the grayscale image to disk as a temporary file
filename = "{}.jpg".format(os.getpid())
cv2.imwrite(filename, gray)

# Load the image as a PIL/Pillow image and apply OCR
# Set the PSM to 6 and the OEM to 3
text = pytesseract.image_to_string(Image.open(filename), config="--psm 6 --oem 3")

# Remove the temporary file
os.remove(filename)

# Write the extracted text to a text file
with open("output.txt", "w") as file:
    file.write(text)

print("Text has been successfully extracted and written to 'output.txt'")
